const observer = new IntersectionObserver(entries => {
    entries.forEach(entry =>{
        if (entry.isIntersecting){
            entry.target.classList.add('fade-in');
            return;
        }
        entry.target.classList.remove('fade-in');
    });
});

var informationDivs = document.getElementsByName("infoBlock");
for (let i = 0; i < informationDivs.length; i++){
    observer.observe(informationDivs[i]);
}

var gifInfo = document.getElementsByName("gifInfo");
for (let i = 0; i < gifInfo.length; i++){
    observer.observe(gifInfo[i]);
}

document.getElementById("clickTextGame1").onclick = function(){
    var currDiv = document.getElementById("expandableDivGame1");
    if (currDiv.style.height === 'auto'){
        currDiv.style.height = '40px';
        this.textContent = 'Click here if you want to learn more about the technical aspects of this project.';
    }
    else
    {
        currDiv.style.height = 'auto';
        this.textContent = 'Technical Information';
    }
}


document.getElementById("clickTextGame2").onclick = function(){
    var currDiv = document.getElementById("expandableDivGame2");
    if (currDiv.style.height === 'auto'){
        currDiv.style.height = '40px';
        this.textContent = 'Click here if you want to learn more about the technical aspects of this project.';
    }
    else
    {
        currDiv.style.height = 'auto';
        this.textContent = 'Technical Information';
    }
}


document.getElementById("clickTextGame3").onclick = function(){
    var currDiv = document.getElementById("expandableDivGame3");
    if (currDiv.style.height === 'auto'){
        currDiv.style.height = '40px';
        this.textContent = 'Click here if you want to learn more about the technical aspects of this project.';
    }
    else
    {
        currDiv.style.height = 'auto';
        this.textContent = 'Technical Information';
    }
}

function hoverCiros(element) {
    element.setAttribute('src', 'Images/NilvorAlt.png');
  }
  
  function unhoverCiros(element) {
    element.setAttribute('src', 'Images/Nilvor.png');
  }

  function hoverTerritorial(element) {
    element.setAttribute('src', 'Images/TerritorialAlt.png');
  }
  
  function unhoverTerritorial(element) {
    element.setAttribute('src', 'Images/Territorial.png');
  }

  function hoverRamps(element) {
    element.setAttribute('src', 'Images/RampsAlt.png');
  }
  
  function unhoverRamps(element) {
    element.setAttribute('src', 'Images/Ramps.png');
  }

  function hoverLemon(element) {
    element.setAttribute('src', 'Images/JohnLemonAlt.png');
  }
  
  function unhoverLemon(element) {
    element.setAttribute('src', 'Images/JohnLemon.png');
  }
  