var rowDivs = document.getElementsByName("row");
var holderDivs = document.getElementsByName("holderDiv");
var impactImageDivs = document.getElementsByName("impactImageDiv");
var gifDivs = document.getElementsByName("gifDiv");
var gifInfoDivs = document.getElementsByName("gifInfoDiv");

var width = holderDivs.offsetWidth;
    
// Insert values on load of page
window.onload = function() {
    // influence sizes onload
    for (let i = 0; i < rowDivs.length; i++){
        // influencing the width of the infoDiv
        if (507 / window.innerWidth > .33){
            var widthPercentage = (507 / window.innerWidth) * 100;
            if (widthPercentage > 50){
                holderDivs[i].style.width = "50%";
                impactImageDivs[i].style.width = "50%";
                // for now this is assuming that every game will have a gif with it
                if (gifDivs.length > 0){
                    gifDivs[i].style.width = "50%";
                    gifInfoDivs[i].style.width = "50%";
                    if (widthPercentage > 70){
                        gifDivs[i].style.width = "100%";
                        gifInfoDivs[i].style.width = "100%";
                    }
                }
                continue;
            }

            holderDivs[i].style.width = widthPercentage + "%";
            var difference = 100 - widthPercentage;
            impactImageDivs[i].style.width = difference + "%";
            if (gifDivs.length > 0){
                gifDivs[i].style.width = widthPercentage + "%";
                gifInfoDivs[i].style.width = difference + "%";
            }
        } else {
            holderDivs[i].style.width = "33%";
            impactImageDivs[i].style.width = "67%";
            if (gifDivs.length > 0){
                gifDivs[i].style.width = "33%";
                gifInfoDivs[i].style.width = "67%";
            }
        }
    }
};
    
// Change values when window is resized
window.onresize = function() {
    // influence sizes onresize
    for (let i = 0; i < rowDivs.length; i++){
        // influencing the width of the infoDiv
        if (507 / window.innerWidth > .33){
            var widthPercentage = (507 / window.innerWidth) * 100;
            if (widthPercentage > 50){
                holderDivs[i].style.width = "50%";
                impactImageDivs[i].style.width = "50%";
                // for now this is assuming that every game will have a gif with it
                if (gifDivs.length > 0){
                    gifDivs[i].style.width = "50%";
                    gifInfoDivs[i].style.width = "50%";
                    if (widthPercentage > 70){
                        gifDivs[i].style.width = "100%";
                        gifInfoDivs[i].style.width = "100%";
                    }
                }
                continue;
            }

            holderDivs[i].style.width = widthPercentage + "%";
            var difference = 100 - widthPercentage;
            impactImageDivs[i].style.width = difference + "%";
            if (gifDivs.length > 0){
                gifDivs[i].style.width = widthPercentage + "%";
                gifInfoDivs[i].style.width = difference + "%";
            }
        } else {
            holderDivs[i].style.width = "33%";
            impactImageDivs[i].style.width = "67%";
            if (gifDivs.length > 0){
                gifDivs[i].style.width = "33%";
                gifInfoDivs[i].style.width = "67%";
            }
        }
    }
};