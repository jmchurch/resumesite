var rowDivs = document.getElementsByName("row");
var holderDivs = document.getElementsByName("holderDiv");
var infoDivs = document.getElementsByName("infoDiv");
var impactImageDivs = document.getElementsByName("impactImageDiv");
var smallImages = document.getElementsByName("smallImage");

    
// Insert values on load of page
window.onload = function() {
    // influence sizes onload
    for (let i = 0; i < rowDivs.length; i++){
        var smallImageHeight = window.innerHeight - infoDivs[i].getBoundingClientRect().bottom; // bottom pixel of the div in relation to the window
        smallImages[i].height = smallImageHeight;
        // influencing the width of the infoDiv
        if (507 / window.innerWidth > .33){
            var widthPercentage = (507 / window.innerWidth) * 100;
            if (widthPercentage > 50){
                holderDivs[i].style.width = "50%";
                impactImageDivs[i].style.width = "50%";
                continue;
            }

            holderDivs[i].style.width = widthPercentage + "%";
            var difference = 100 - widthPercentage;
            impactImageDivs[i].style.width = difference + "%";
        } else {
            holderDivs[i].style.width = "33%";
            impactImageDivs[i].style.width = "67%"
        }
    }
};
    
// Change values when window is resized
window.onresize = function() {
    // influence sizes onresize
    for (let i = 0; i < rowDivs.length; i++){
        var smallImageHeight = window.innerHeight - infoDivs[i].getBoundingClientRect().bottom; // bottom pixel of the div in relation to the window
        smallImages[i].height = smallImageHeight;

        // influencing the width of the infoDiv
        if (507 / window.innerWidth > .33){
            var widthPercentage = (507 / window.innerWidth) * 100;
            if (widthPercentage > 50){
                holderDivs[i].style.width = "50%";
                impactImageDivs[i].style.width = "50%";
                continue;
            }

            holderDivs[i].style.width = widthPercentage + "%";
            var difference = 100 - widthPercentage;
            impactImageDivs[i].style.width = difference + "%";
        } else {
            holderDivs[i].style.width = "33%";
            impactImageDivs[i].style.width = "67%"
        }
    }
};